#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

enum Recommendation {
    BALANCED_CRYSTALLOID_SOLUTION_GT_60,
    PLASMAPHERESIS,
    EARLY_PROLONGED_EPIDURAL_ANALGESIA,
    STENTING,
    ENDOSCOPIC_PAPILLOSPHINCTEROTOMY,
    EARLY_ENTERAL_NUTRITION
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void calculate();
};

#endif // MAINWINDOW_H
