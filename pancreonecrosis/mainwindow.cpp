#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSet>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    statusBar()->showMessage("© 2024 Туровец М. И., Стрельцов В. О.");

    connect(ui->srcSex, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });
    connect(ui->srcAge, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculate(); });

    connect(ui->srcDiabetes, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });
    connect(ui->srcHOBL, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });
    connect(ui->srcIBS, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });
    connect(ui->srcPIKS, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });
    connect(ui->srcChronicKidneyDisease, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });

    connect(ui->srcAlcoholAlimentaryPN, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });

    connect(ui->srcSKF, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculate(); });

    connect(ui->srcSurgery, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });

    connect(ui->srcBalancedCrystalloidSolutions, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });
    connect(ui->srcPlasmapheresis, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });
    connect(ui->srcProlongedEpiduralAnalgesia, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });
    connect(ui->srcEnteralNutrition, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculate(); });

    calculate();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::calculate()
{
    // QMessageBox Msgbox;
    // int sum;
    // Msgbox.setText("calc");
    // Msgbox.exec();
    QSet<Recommendation> recommendations;

    bool diabetesIsSet = ui->srcDiabetes->currentIndex() > 0;
    bool hoblIsSet = ui->srcHOBL->currentIndex() > 0;
    bool ibsIsSet = ui->srcIBS->currentIndex() > 0;
    bool piksIsSet = ui->srcPIKS->currentIndex() > 0;
    bool ckdIsSet = ui->srcChronicKidneyDisease->currentIndex() > 0;
    bool aapnIsSet = ui->srcAlcoholAlimentaryPN->currentIndex() > 0;
    bool surgeryIsSet = ui->srcSurgery->currentIndex() > 0;
    bool balancedCrystalloidSolutionsIsSet = ui->srcBalancedCrystalloidSolutions->currentIndex() > 0;
    bool plasmapheresisIsSet = ui->srcPlasmapheresis->currentIndex() > 0;
    bool prolongedEpiduralAnalgesiaIsSet = ui->srcProlongedEpiduralAnalgesia->currentIndex() > 0;
    bool enternalNutritionIsSet = ui->srcEnteralNutrition->currentIndex() > 0;

    bool isMale = ui->srcSex->currentIndex() == 0;
    double age = (double)ui->srcAge->value();
    double diabetes = (double)(ui->srcDiabetes->currentIndex() == 1 ? 1.0 : 0.0);
    double hobl = (double)(ui->srcHOBL->currentIndex() == 1 ? 1.0 : 0.0);
    double ibs = (double)(ui->srcIBS->currentIndex() == 1 ? 1.0 : 0.0);
    double piks = (double)(ui->srcPIKS->currentIndex() == 1 ? 1.0 : 0.0);
    double ckd = (double)(ui->srcChronicKidneyDisease->currentIndex() == 1 ? 1.0 : 0.0);
    double aapn = (double)(ui->srcAlcoholAlimentaryPN->currentIndex() == 1 ? 1.0 : 0.0);
    double skf = (double)(ui->srcSKF->value());
    double laparotomy = (double)(ui->srcSurgery->currentIndex() == 2 ? 1.0 : 0.0);
    double laparoscopy = (double)(ui->srcSurgery->currentIndex() == 3 ? 1.0 : 0.0);
    double laparoscopicCholecystoscopy = (double)(ui->srcSurgery->currentIndex() == 4 ? 1.0 : 0.0);
    double stenting = (double)(ui->srcSurgery->currentIndex() == 5 ? 1.0 : 0.0);
    double epst = (double)(ui->srcSurgery->currentIndex() == 6 ? 1.0 : 0.0);
    double balancedCrystalloidSolutions = (double)(ui->srcBalancedCrystalloidSolutions->currentIndex() == 1 ? 1.0 : 0.0);
    double plasmapheresis = (double)(ui->srcPlasmapheresis->currentIndex() == 1 ? 1.0 : 0.0);
    double prolongedEpiduralAnalgesia = (double)(ui->srcProlongedEpiduralAnalgesia->currentIndex() == 1 ? 1.0 : 0.0);
    double enternalNutrition = (double)(ui->srcEnteralNutrition->currentIndex() == 1 ? 1.0 : 0.0);

    if (ibsIsSet && diabetesIsSet && hoblIsSet && ckdIsSet && surgeryIsSet
        && balancedCrystalloidSolutionsIsSet && prolongedEpiduralAnalgesiaIsSet && plasmapheresisIsSet
    ) {
        double probOPP = 1.0 / (1.0 + exp(
            0.761
            - (age * 0.035)
            + (ibs * 1.073)
            + (diabetes * 1.492)
            + (hobl * 1.104)
            - (ckd * 1.789)
            - (laparotomy * 2.863)
            - (laparoscopy * 1.186)
            - (laparoscopicCholecystoscopy * 1.291)
            - (stenting * 2.109)
            + (balancedCrystalloidSolutions * 2.481)
            + (skf * 0.044)
            + (prolongedEpiduralAnalgesia * 0.868)
            + (plasmapheresis * 0.887)
            ));
        probOPP = fmax(0, fmin(probOPP, 1.0));
        ui->labResOPP->setText(QString("Острое повреждение почек: %1%").arg(probOPP * 100, 0, 'f', 2));
        if (probOPP >= 0.094) {
            recommendations.insert(Recommendation::BALANCED_CRYSTALLOID_SOLUTION_GT_60);
            recommendations.insert(Recommendation::PLASMAPHERESIS);
            recommendations.insert(Recommendation::EARLY_PROLONGED_EPIDURAL_ANALGESIA);
        }
    } else {
        ui->labResOPP->setText(QString("Острое повреждение почек: недостаточно данных для расчёта"));
    }

    if (hoblIsSet && ckdIsSet && surgeryIsSet) {
        double probORDS = 1.0 / (1.0 + exp(
             2.504
             - (hobl * 1.253)
             - (ckd * 0.791)
             - (laparotomy * 2.998)
             - (laparoscopy * 1.258)
             - (laparoscopicCholecystoscopy * 1.893)
             + (skf * 0.025)
             ));
        probORDS = fmax(0, fmin(probORDS, 1.0));
        ui->labResORDS->setText(QString("Острый респираторный дистресс-синдром: %1%").arg(probORDS * 100, 0, 'f', 2));
        if (probORDS >= 0.095) {
            recommendations.insert(Recommendation::PLASMAPHERESIS);
            if (ui->srcAlcoholAlimentaryPN->currentIndex() == 1) {
                recommendations.insert(Recommendation::STENTING);
            } else {
                recommendations.insert(Recommendation::ENDOSCOPIC_PAPILLOSPHINCTEROTOMY);
            }
            recommendations.insert(Recommendation::EARLY_PROLONGED_EPIDURAL_ANALGESIA);
        }
    } else {
        ui->labResORDS->setText(QString("Острый респираторный дистресс-синдром: недостаточно данных для расчёта"));
    }

    if (surgeryIsSet && balancedCrystalloidSolutionsIsSet && prolongedEpiduralAnalgesiaIsSet && enternalNutritionIsSet && plasmapheresisIsSet) {
        double probSPON = 1.0 / (1.0 + exp(
             3.753
             - (age * 0.112)
             + (laparoscopy * 2.693)
             + (laparoscopicCholecystoscopy * 2.479)
             + (stenting * 2.306)
             + (balancedCrystalloidSolutions * 0.922)
             + (skf * 0.042)
             + (prolongedEpiduralAnalgesia * 2.271)
             + (enternalNutrition * 3.729)
             + (plasmapheresis * 3.166)
             ));
        probSPON = fmax(0, fmin(probSPON, 1.0));
        ui->labResSPON->setText(QString("Синдром полиорганной недостаточности: %1%").arg(probSPON * 100, 0, 'f', 2));
        if (probSPON >= 0.054) {
            recommendations.insert(Recommendation::PLASMAPHERESIS);
            if (ui->srcAlcoholAlimentaryPN->currentIndex() == 1) {
                recommendations.insert(Recommendation::STENTING);
            } else {
                recommendations.insert(Recommendation::ENDOSCOPIC_PAPILLOSPHINCTEROTOMY);
            }
            recommendations.insert(Recommendation::EARLY_PROLONGED_EPIDURAL_ANALGESIA);
            recommendations.insert(Recommendation::EARLY_ENTERAL_NUTRITION);
        }
    } else {
        ui->labResSPON->setText(QString("Синдром полиорганной недостаточности: недостаточно данных для расчёта"));
    }

    if (piksIsSet && diabetesIsSet && ckdIsSet && aapnIsSet && surgeryIsSet && prolongedEpiduralAnalgesiaIsSet
        && enternalNutritionIsSet && plasmapheresisIsSet
    ) {
        double probInfection = 1.0 / (1.0 + exp(
              0.12
              - (isMale ? 0.660 : 0)
              - (piks * 1.023)
              - (diabetes * 0.923)
              - (ckd * 1.034)
              + (aapn * 1.114)
              - (laparotomy * 1.659)
              + (laparoscopy * 1.670)
              + (prolongedEpiduralAnalgesia * 1.371)
              + (enternalNutrition * 0.828)
              + (plasmapheresis * 0.795)
              ));
        probInfection = fmax(0, fmin(probInfection, 1.0));
        ui->labResInfection->setText(QString("Инфицирование: %1%").arg(probInfection * 100, 0, 'f', 2));
        if (probInfection >= 0.243) {
            recommendations.insert(Recommendation::PLASMAPHERESIS);
            recommendations.insert(Recommendation::EARLY_PROLONGED_EPIDURAL_ANALGESIA);
            recommendations.insert(Recommendation::EARLY_ENTERAL_NUTRITION);
        }
    } else {
        ui->labResInfection->setText(QString("Инфицирование: недостаточно данных для расчёта"));
    }

    QStringList recs;
    if (recommendations.contains(Recommendation::BALANCED_CRYSTALLOID_SOLUTION_GT_60)) {
        recs.append("• Доля сбалансированных кристаллоидных растворов более 60%");
    }
    if (recommendations.contains(Recommendation::PLASMAPHERESIS)) {
        recs.append("• Плазмаферез");
    }
    if (recommendations.contains(Recommendation::EARLY_PROLONGED_EPIDURAL_ANALGESIA)) {
        recs.append("• Ранняя продлённая грудная эпидуральная анальгезия");
    }
    if (recommendations.contains(Recommendation::STENTING)) {
        recs.append("• Стентирование вирсунгова протока");
    }
    if (recommendations.contains(Recommendation::ENDOSCOPIC_PAPILLOSPHINCTEROTOMY)) {
        recs.append("• Эндоскопическая папиллосфинктеротомия");
    }
    if (recommendations.contains(Recommendation::EARLY_ENTERAL_NUTRITION)) {
        recs.append("• Раннее энтеральное питание");
    }
    if (recommendations.isEmpty()) {
        recs.append("• Стандартная терапия");
    }
    ui->labResRecommendations->setText(recs.join("\n"));
}
