#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    statusBar()->showMessage("© 2019 Туровец М. И., Стрельцов В. О.");

    calculateOpp();
    calculateErhpg();
    calculateSurgery();
    calculatePankreo();

    connect(ui->oppAge, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculateOpp(); });
    connect(ui->oppSex, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculateOpp(); });
    connect(ui->oppDisease, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculateOpp(); });
    connect(ui->oppBilirubin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double index){ calculateOpp(); });
    connect(ui->oppAmilaza, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double index){ calculateOpp(); });

    connect(ui->erhpgAmilaza, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){ calculateErhpg(); });
    connect(ui->erhpgHrBefore, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculateErhpg(); });
    connect(ui->erhpgHrAfter, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculateErhpg(); });
    connect(ui->erhpgLeykoBefore, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){ calculateErhpg(); });
    connect(ui->erhpgLeykoAfter, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){ calculateErhpg(); });
    connect(ui->erhpgPain, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculateErhpg(); });

    connect(ui->surgeryOperation, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculateSurgery(); });
    connect(ui->surgeryAge, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculateSurgery(); });
    connect(ui->surgeryOrder, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculateSurgery(); });
    connect(ui->surgeryAsa, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculateSurgery(); });
    connect(ui->surgeryMassIndex, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index){ calculateSurgery(); });

    connect(ui->pankAmilazaNow, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){ calculatePankreo(); });
    connect(ui->pankAmilazaBefore, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){ calculatePankreo(); });
    connect(ui->pankHrNow, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculatePankreo(); });
    connect(ui->pankHrBefore, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculatePankreo(); });
    connect(ui->pankLeykoNow, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){ calculatePankreo(); });
    connect(ui->pankLeykoBefore, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double value){ calculatePankreo(); });
    connect(ui->pankKreatininNow, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculatePankreo(); });
    connect(ui->pankKreatininBefore, QOverload<int>::of(&QSpinBox::valueChanged), [=](int value){ calculatePankreo(); });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::calculateOpp()
{
    bool isMale = ui->oppSex->currentIndex() == 0;
    int age = ui->oppAge->value();
    double amilaza = ui->oppAmilaza->value();
    double bilirubin = ui->oppBilirubin->value();

    // Риск развития ОПП
    double A1 = amilaza;
    double An = 220;
    double B1 = bilirubin;
    double C1 = age;
    double D = isMale ? 1 : 2;
    double E = 1;
    switch (ui->oppDisease->currentIndex()) {
    case 0:
    case 3:
        E = 2;
        break;
    case 1:
    case 2:
    case 4:
    case 5:
    case 7:
        E = 3;
        break;
    case 6:
        E = 1;
        break;
    }
    QString risk = "";
    if (C1 > 0) {
        double res = (A1*2/An + 50/B1)*(40/C1)*D*E;
        if (res < 0.5) {
            risk = "< 2%";
        } else if (res < 1) {
            risk = "2-10%";
        } else if (res < 2) {
            risk = "11-20%";
        } else if (res < 4) {
            risk = "21-30%";
        } else if (res <= 5) {
            risk = "31-40%";
        } else if (res > 5) {
            risk = "> 40%";
        }
    }

    // Коэффициенты риска
    int sum = 0;
    if (age <= 30) {
        sum += isMale ? 0 : -1;
    } else if (age <= 50) {
        sum += 1;
    } else if (age <= 70) {
        sum += isMale ? -1 : 1;
    } else {
        sum -= 1;
    }

    switch (ui->oppDisease->currentIndex()) {
    case 0:
        sum += 1;
        break;
    case 1:
        sum += 3;
        break;
    case 2:
        sum += 3;
        break;
    case 3:
        sum -= 2;
        break;
    case 4:
        sum += isMale ? 3 : 2;
        break;
    case 5:
        sum += 1;
        break;
    case 6:
        sum -= 1;
        break;
    case 7:
        sum -= isMale ? 2 : 0;
        break;
    }

    if (bilirubin <= 21.6) {
        sum += isMale ? -1 : 1;
    } else if (bilirubin <= 100) {
        sum -= isMale ? 1 : 2;
    } else if (bilirubin <= 200) {
        sum += 1;
    } else if (bilirubin) {
        sum += 1;
    }

    if (amilaza <= 100) {
        sum -= 1;
    } else if (amilaza <= 200) {
        sum += isMale ? 1 : 2;
    } else {
        sum += 3;
    }

    ++sum;

    ui->oppResult->setText("Риск развития ОПП: " + risk + "\n" + (sum >= 1 ? "Показана ГЭА" : "Не показана ГЭА"));
}

void MainWindow::calculateErhpg()
{
    double Ap = ui->erhpgAmilaza->value();
    double An = 220;
    double hr0 = ui->erhpgHrBefore->value();
    double hr1 = ui->erhpgHrAfter->value();
    double l0 = ui->erhpgLeykoBefore->value();
    double l1 = ui->erhpgLeykoAfter->value();
    double kb = ui->erhpgPain->currentIndex();
    double res = ((Ap / An) * (hr1/hr0) * (l1/l0) * (kb+1)) / 4;
    ui->erhpgResult->setText(QString(res >= 1 ? "Панкреатит" : "Транзиторная гиперамилаземия") + " (ИП = " + QString::number(res) + ")");
}

void MainWindow::calculateSurgery()
{
    int sum = 0;
    switch (ui->surgeryOperation->currentIndex()) {
    case 0:
        break;
    case 1:
        sum += 1;
        break;
    case 2:
        sum += 3;
        break;
    }
    switch (ui->surgeryAge->currentIndex()) {
    case 0:
        break;
    case 1:
        sum += 1;
        break;
    case 2:
        sum += 2;
        break;
    case 3:
        sum += 3;
        break;
    }
    switch (ui->surgeryOrder->currentIndex()) {
    case 0:
        break;
    case 1:
        sum += 1;
        break;
    }
    switch (ui->surgeryAsa->currentIndex()) {
    case 0:
        break;
    case 1:
        sum += 1;
        break;
    }
    switch (ui->surgeryMassIndex->currentIndex()) {
    case 0:
        break;
    case 1:
        sum += 3;
        break;
    }
    if (sum <= 1) {
        ui->surgeryResult->setText("Риск развития ОПП 0-10%");
    } else if (sum <= 3) {
        ui->surgeryResult->setText("Риск развития ОПП 11-20%");
    } else if (sum <= 5) {
        ui->surgeryResult->setText("Риск развития ОПП 21-50%");
    } else {
        ui->surgeryResult->setText("Риск развития ОПП 51-100%");
    }
}

void MainWindow::calculatePankreo()
{
    double A0 = ui->pankAmilazaBefore->value();
    double A1 = ui->pankAmilazaNow->value();
    double hr0 = ui->pankHrBefore->value();
    double hr1 = ui->pankHrNow->value();
    double l0 = ui->pankLeykoBefore->value();
    double l1 = ui->pankLeykoNow->value();
    double k0 = ui->pankKreatininBefore->value();
    double k1 = ui->pankKreatininNow->value();
    double res = (A1/A0)*(hr1/hr0)*(l1/l0)*(k1/k0);
    ui->pankResult->setText(res < 1 ? "Положительная динамика" : "Отрицательная динамика");
}

